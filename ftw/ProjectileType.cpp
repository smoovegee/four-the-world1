#include "stdafx.h"
#include "ProjectileType.h"



ProjectileType::ProjectileType()
{
}

ProjectileType::~ProjectileType()
{
}

void ProjectileType::initialise(Sprite* projectileSprite, float projectileDamage, float projectileRange, float width, float height) {

	sprite = projectileSprite;
	damage = projectileDamage;
	range = projectileRange;
	w = width;
	h = height;
}

float ProjectileType::getDamage() {

	return damage = 10;
}

float ProjectileType::getRange() {

	return range;
}

float ProjectileType::getWidth() {

	return w=10.2f;  //changed the size of the bullet
}

float ProjectileType::getHeight() {

	return h=10.2f;
}

void ProjectileType::draw(SDL_Renderer* renderer, Float2 pos, float orientation) {

	SDL_Rect targetRect;

	targetRect.x = int(pos.x);
	targetRect.y = int(pos.y);
	targetRect.w = int(w);
	targetRect.h = int(h);

	sprite->draw(renderer, &targetRect, orientation);
}
