#include "SDL.h"
#include "stdafx.h"
#include "stdio.h"
#include "FTWGame.h"




FTWGame::FTWGame() {
}


FTWGame::~FTWGame() {
}


void FTWGame::initialise() {

	const int SDL_OKAY = 0;

	int sdlStatus = SDL_Init(SDL_INIT_EVERYTHING);

	if (sdlStatus != SDL_OKAY)
		throw "SDL init error";


	
	gameWindow = SDL_CreateWindow(
		"Four the World Demo",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		800,
		600,
		SDL_WINDOW_SHOWN);

	


	// Enumerate render drivers
	int n = SDL_GetNumRenderDrivers();

	printf("Number of renderers = %d\n", n);

	for (int i = 0; i < n; ++i) {

		SDL_RendererInfo renderInfo;

		if (SDL_GetRenderDriverInfo(i, &renderInfo) == 0) { // 0 means success

			printf("Renderer %d : %s\n", i, renderInfo.name);
		}
	}
	

	// Use first (Default) renderer - this is usually Direct3D based
	
	gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

	


	// -----------------------.png

	background = new Sprite();
	background->initialise(gameRenderer, "Assets\\Images\\map.png");

	tinaSprite = new Sprite();
	tinaSprite->initialise(gameRenderer, "Assets\\Images\\tina.png");

	enemySprite1 = new Sprite();
	enemySprite1->initialise(gameRenderer, "Assets\\Images\\enemy2.png");

	enemySprite2 = new Sprite();
	enemySprite2->initialise(gameRenderer, "Assets\\Images\\enemy.png");

	enemySprite3 = new Sprite();
	enemySprite3->initialise(gameRenderer, "Assets\\Images\\enemy.png");

	mainPlayer = new Player();
	mainPlayer->initialise(tinaSprite, 100, 100, 100.0f);

	// Recipe 5 & 9 - instantiate something to collide with and shoot
	Zords = new Enemy();
	Zords->initialise(enemySprite1, 300, 400); //changes position

	Grottos = new Enemy();
	Grottos->initialise(enemySprite2, 700, 400); //changes position

	Boss1 = new Enemy();
	Boss1->initialise(enemySprite3, 500, 400); //changes position



	// Recipe 9 - Bullets
	bulletSprite = new Sprite();
	bulletSprite->initialise(gameRenderer, "Assets\\Images\\projectile01.png");

	bulletType = new ProjectileType();
	bulletType->initialise(bulletSprite, 5, 600, 32, 32);

	for (int i = 0; i < MAX_BULLETS; i++) {

		bullets[i] = nullptr;
	}

	// Add other initialisation code here...

}

void FTWGame::runGameLoop() {

	gameRunning = true;

	while (gameRunning) {

		// Recipe 3 - Update timing / clock

		// Calculate time elapsed
		currentTimeIndex = SDL_GetTicks();
		timeDelta = currentTimeIndex - prevTimeIndex;
		timeDeltaInSeconds = float(timeDelta) / 1000.0f;

		// Store current time index into prevTimeIndex for next frame
		prevTimeIndex = currentTimeIndex;

		// --------------------

		handleEvents();
		update();
		draw();
	}
}

void FTWGame::handleEvents() {

	SDL_Event event;

	// Recipe 4 - device and instance id variables for controller connections / removal
	Sint32 deviceID;
	Sint32 instanceID;

	// Check for next event
	while (SDL_PollEvent(&event)) {

		switch (event.type) {

			// Check if window closed
		case SDL_QUIT:
			gameRunning = false;
			break;

			// Key pressed event
		case SDL_KEYDOWN:

			// Toggle key states based on key pressed
			switch (event.key.keysym.sym) {

			case SDLK_UP:
				keyState |= Keys::Up;
				break;

			case SDLK_DOWN:
				keyState |= Keys::Down;
				break;

			case SDLK_LEFT:
				keyState |= Keys::Left;
				break;

			case SDLK_RIGHT:
				keyState |= Keys::Right;
				break;

			case SDLK_SPACE:
				keyState |= Keys::Fire;
				break;

			case SDLK_ESCAPE:
				gameRunning = false;
				break;
			}
			break;

			// Key released event
		case SDL_KEYUP:

			switch (event.key.keysym.sym)
			{
			case SDLK_UP:
				keyState &= (~Keys::Up);
				break;

			case SDLK_DOWN:
				keyState &= (~Keys::Down);
				break;

			case SDLK_LEFT:
				keyState &= (~Keys::Left);
				break;

			case SDLK_RIGHT:
				keyState &= (~Keys::Right);
				break;

			case SDLK_SPACE:
				keyState &= (~Keys::Fire);

				// Find first "free" bullet
				if (1) {

					int i = 0;
					while (bullets[i] != nullptr && i < MAX_BULLETS) {
						i++;
					}

					if (i < MAX_BULLETS) {

						bullets[i] = new BulletInstance();
						Float2 pos = mainPlayer->getPosition();
						pos.x += mainPlayer->getWidth() >> 1; //where the bullets come out in the middle of sprite
						pos.y += mainPlayer->getHeight() >> 1;
						bullets[i]->initialise(bulletType, pos, Float2(200.0f, 0.0f));
					}
				}

				break;
			}

			break;


			//
			// Recipe 4 - Controller input
			//

		case SDL_CONTROLLERDEVICEADDED:

			deviceID = event.cdevice.which;
			printf("Controller device %d added to system\n", deviceID);

			// Setup UP TO 4 controllers for this game!
			if (deviceID >= 0 && deviceID < 4) {

				// Open controller for processing - deviceID indexes the ith controller on the system
				controllers[deviceID] = SDL_GameControllerOpen(deviceID);
			}
			break;

		case SDL_CONTROLLERDEVICEREMOVED:

			instanceID = event.cdevice.which;

			printf("Controller instance_id %d removed\n", instanceID);

			if (1) {

				SDL_GameController *controller = SDL_GameControllerFromInstanceID(instanceID);

				if (controller != nullptr) {

					SDL_GameControllerClose(controller);
				}
			}

			break;


		case SDL_CONTROLLERAXISMOTION:

			instanceID = event.caxis.which;

			if (instanceID == 0) {

				// mainPlayer only updated by controller with instanceID = 0
				Uint8 axisID = event.caxis.axis;

				// Implement "Dead Zone" of 7849 to avoid analogue noise creating suprious movement
				const Sint16 deadZone = 7849;

				switch (axisID) {


				case SDL_CONTROLLER_AXIS_LEFTX:
					lx = (abs(event.caxis.value) < deadZone) ? 0 : event.caxis.value;
					break;

				case SDL_CONTROLLER_AXIS_LEFTY:
					ly = (abs(event.caxis.value) < deadZone) ? 0 : event.caxis.value;
					break;

				case SDL_CONTROLLER_AXIS_RIGHTX:
					rx = (abs(event.caxis.value) < deadZone) ? 0 : event.caxis.value;
					break;
				}
			}
			break;

		case SDL_CONTROLLERBUTTONDOWN:

			instanceID = event.cbutton.which;

			if (instanceID == 0) {

				// Find first "free" bullet
				int i = 0;
				while (bullets[i] != nullptr && i < MAX_BULLETS) {
					i++;
				}

				if (i < MAX_BULLETS) {

					bullets[i] = new BulletInstance();
					bullets[i]->initialise(bulletType, mainPlayer->getPosition(), Float2(200.0f, 0.0f));
				}
			}

			break;

		case SDL_CONTROLLERBUTTONUP:

			instanceID = event.cbutton.which;

			if (instanceID == 0) {

				printf("id:%d button:%d status:%d\n", instanceID, event.cbutton.button, event.cbutton.state);
			}

			break;
		}
	}


}


void FTWGame::update() {

	// FOR NOW USE timeDeltaInSeconds DIRECTLY - NOT IDEAL AS ERROR CAN ACCUMULATE AS DISCUSSED IN THE LECTURE!

	// Move mainPlayer based on key flags
	float xMovement = 0.0f;
	float yMovement = 0.0f;
	float rotation = 0.5f; // had to change because sprite wouldnt stand straight

	// Recipe 4 - Check if controller state updated - if not default to keyboard
	if (lx != 0 || ly != 0 || rx != 0) {

		xMovement = float(lx) / 32768.0f * 100.0f;
		yMovement = float(ly) / 32768.0f * 100.0f;
		rotation = float(rx) / 32768.0f * (360.0f / 60.0f);
	}
	else {

		if (keyState & Keys::Left)
			xMovement = -100.0f;
		else if (keyState & Keys::Right)
			xMovement = 100.0f;

		if (keyState & Keys::Up)
			yMovement = -100.0f;
		else if (keyState & Keys::Down)
			yMovement = 100.0f;

		// Ensure length of direction vector is consistent for axis-aligned and diagonal movement
		float dx = float(xMovement);
		float dy = float(yMovement);

		float dLength = sqrtf(dx * dx + dy * dy);

		if (dLength > 0.0f) {

			dx = dx / dLength;
			dy = dy / dLength;

			dx *= 100.0f;
			dy *= 100.0f;

			xMovement = dx;
			yMovement = dy;
		}
	}

	mainPlayer->move(xMovement * timeDeltaInSeconds, yMovement * timeDeltaInSeconds);
	mainPlayer->rotate(rotation * timeDeltaInSeconds); // Recipe 4 - rotate player based on joystick input


													   // Recipe 9 - Update bullets
	for (int i = 0; i < MAX_BULLETS; i++) {

		if (bullets[i]) {
			bullets[i]->update(timeDeltaInSeconds);
		}
	}


	// Recipe 5 - Check collisions

	// Simple check between two objects
	if (AABB::intersectAABB(mainPlayer->getBoundingBox(), Zords->getBoundingBox())) {

		// A hit!
		mainPlayer->addHealth(-0.1f);

		if (mainPlayer->getHealth() <= 0.0f) {

			printf("GAME OVER!!!\n");
			gameRunning = false;
		}
	}

	if (AABB::intersectAABB(mainPlayer->getBoundingBox(), Grottos->getBoundingBox())) {

		// A hit!
		mainPlayer->addHealth(-0.1f);

		if (mainPlayer->getHealth() <= 0.0f) {

			printf("GAME OVER!!!\n");
			gameRunning = false;
		}
	}
	if (AABB::intersectAABB(mainPlayer->getBoundingBox(), Boss1->getBoundingBox())) {  //bounding box for all enemies

		// A hit!
		mainPlayer->addHealth(-0.1f);

		if (mainPlayer->getHealth() <= 0.0f) {
				printf("GAME OVER!!!\n");
			gameRunning = false;
		}
	}



	// Recipe 9 - Check for bullet collisions with 'Zords' Enemy object
	for (int i = 0; i < MAX_BULLETS; i++) {
		
		if (bullets[i]) {

			if (AABB::intersectAABB(bullets[i]->getBoundingBox(), Zords->getBoundingBox())) {
				bullets[i]->hit(Zords), Zords->updateHealth(0);
				printf("Hit!\n");
				if (Zords->getHealth() <= -0.0f) {
					Zords->initialise(enemySprite1, 800, 800);
						Zords->getBoundingBox();

					
					}
				}
			}
		}
	// Recipe 9 - Check for bullet collisions with 'Grottos' Enemy object
	for (int i = 0; i < MAX_BULLETS; i++) {

		if (bullets[i]) {

			if (AABB::intersectAABB(bullets[i]->getBoundingBox(), Grottos->getBoundingBox())) {
				bullets[i]->hit(Grottos), Grottos->updateHealth(0);
				printf("Hit!\n");
				if (Grottos->getHealth() <= -0.0f) {
					Grottos->initialise(enemySprite2, 800, 800);
					Grottos->getBoundingBox();


				}
			}
		}
	}

	// Recipe 9 - Check for bullet collisions with 'Boss1' Enemy object
	for (int i = 0; i < MAX_BULLETS; i++) {

		if (bullets[i]) {

			if (AABB::intersectAABB(bullets[i]->getBoundingBox(), Boss1->getBoundingBox())) {
				bullets[i]->hit(Boss1), Boss1->updateHealth(0);
				printf("Hit!\n");
				if (Boss1->getHealth() <= -0.0f) {
					Boss1->initialise(enemySprite3, 800, 800);
					Boss1->getBoundingBox();
				}
			}
		}
	}
	// Recipe 9 - Check for bullets out of range - delete when found
	for (int i = 0; i < MAX_BULLETS; i++) {

		if (bullets[i] && bullets[i]->exceededRange()) {

			printf("eol!\n");
			delete bullets[i];
			bullets[i] = nullptr;
				
				
			}
		}
	}


void FTWGame::draw() {

	// 1. Clear the screen
	
	SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255); // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
	SDL_RenderClear(gameRenderer);



	// 2. Draw the scene...
	
	SDL_Rect targetRect;
	targetRect.x = 0;
	targetRect.y = 0;
	targetRect.w = 800;
	targetRect.h = 800;

	background->draw(gameRenderer, &targetRect, 0.0f); //draws the background

	// Draw the main player
	mainPlayer->draw(gameRenderer);

	// Recipe 5 - instantiate something to collide against
	if (Zords != nullptr) {
		Zords->draw(gameRenderer);
	}
	Grottos->draw(gameRenderer);
	Boss1->draw(gameRenderer);

	// Recipe 9 - Draw bullets
	for (int i = 0; i < MAX_BULLETS; i++) {

		if (bullets[i]) {
			bullets[i]->draw(gameRenderer);
		}
	}


	// 3. Present the current frame to the screen
	SDL_RenderPresent(gameRenderer);
}
