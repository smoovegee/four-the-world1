// main.cpp : Defines the entry point for the console application.
//
#include "stdafx.h" // MUST BE THE FIRST LINE OF A CPP FILE
#include "FTWGame.h"
#include <SDL.h> 



int main(int argc, char* args[])
{
	FTWGame* mainGame = new FTWGame();

	mainGame->initialise();
	mainGame->runGameLoop();


	return 0; //important must have
}
