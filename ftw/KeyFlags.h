#pragma once


#include <cstdint> //libary needed for keyboard


	typedef uint32_t KeyFlags; //each bit represents a key pressed

	enum Keys {

		Up      = 0b00001,
		Down    = 0b00010,
		Left    = 0b00100,
		Right   = 0b01000,
		Fire    = 0b10000,
	};


