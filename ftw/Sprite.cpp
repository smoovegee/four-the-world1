#include "stdafx.h"
#include "Sprite.h"
#include <SDL_image.h>


Sprite::Sprite()
{
}


Sprite::~Sprite()
{
}

void Sprite::initialise(SDL_Renderer* renderer, const char *pathToImage) {

	// Load image to the screen
	SDL_Surface* image = IMG_Load(pathToImage);

	//Its creates a textured object
	texture = SDL_CreateTextureFromSurface(renderer, image);

	sourceRectangle.x = 0;
	sourceRectangle.y = 0;
	SDL_QueryTexture(texture, 0, 0, &(sourceRectangle.w), &(sourceRectangle.h));

	// Clean-up - we're done with 'image' now our texture has been created
	SDL_FreeSurface(image);

	IMG_Quit();
}


void Sprite::draw(SDL_Renderer* renderer, SDL_Rect* targetRect, float renderOrientation) {

	SDL_RenderCopyEx(renderer, texture, &sourceRectangle, targetRect, renderOrientation, 0, SDL_FLIP_NONE);

	IMG_Quit();
	
}