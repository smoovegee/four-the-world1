#pragma once

class Sprite
{
	SDL_Texture*	texture = nullptr;
	SDL_Rect		sourceRectangle;

public:
	Sprite();
	~Sprite();

	void initialise(SDL_Renderer* renderer, const char *pathToImage);
	void draw(SDL_Renderer* renderer, SDL_Rect* targetRect, float renderOrientation); //Adds rotation to sprite rendering
};


